var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

var app = express();

var destinationServer = "http://localhost:18080";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("content-type","application/json; charset=utf-8");
  next();
});

app.get('*', function (req, res, next) {
  // res.writeHead(200, {"Content-Type": "text/plain"});
  // res.end("Hello World\n");

  httpGetAsync(destinationServer+req.originalUrl,req, function(text){
    res.send(text.responseText)
  });
});

function httpGetAsync(theUrl, req, callback)
{
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function() { 
        if (xml.readyState == 4 && xml.status == 200)
            callback(xml);
    }
    xml.open("GET", theUrl, true);
   // xml.setRequestHeader(req.rawHeaders)
    xml.send(null);
}

app.use(function (err, req, res, next) {
  console.log(err)
  res.sendStatus(500);
});

app.listen(process.env.PORT, function () {
  // console.log("Wystartowalem na porcie 1234...");
});