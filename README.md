# README #

###Zanim zaczniesz…###
W folderze Pliki_do_instalacji powinny znajdować się następujące pliki niezbędne do uruchomienia ISS w połączeniu ze stworzonym bypass’em:

* 2x certyfikaty
* Pliki server.js oraz Web.config w folderze Sites
* Urlrewrite.exe 

###Przygotowanie Bypass’a###
Wejdź do głównego folderu, a następnie w konsoli wpisz: npm install 

Jeśli coś pójdzie nie tak warto wpisać komendę z uprawnieniami administratora.

###Przygotowanie IIS###

1. 	Uruchom aplikację systemową wppwiz.cpl
2.	W bocznym pasku wybierz: Włącz lub wyłącz funkcje systemu Windows
3.	Z gałęzi Internetowe usługi informacyjne doinstaluj:

* Narzędzia zarz. Siecią Web (Konsola, Narzędzia i skrypty oraz Usługa zarz.)
* Usługi WWW (wszystko w gałęzi oprócz Kompresja zawartości dynamicznej, publ. WebDAV, zabezpieczeń; z zabezp. powinno zostać zaznaczone tylko filtrowanie żądań)

4. 	Zainstaluj: https://github.com/Azure/iisnode/releases/download/v0.2.21/iisnode-full-v0.2.21-x64.msi 
5.	Uruchom IIS, a następnie usuń Default site z folderu Witryny
6.	Utwórz folder Sites na dysku, w nim kolejny: HMAProxy, a kolejno w nim 2 pliki: server.js oraz Web.config
7.	Zainstaluj certyfikaty wybierając poniższe opcje:

*	User lokalny->oznacz jako eksportowany->umieść certyf. W zaufanych wydawcach

8.	Wejdź w plik C:/Windows/System32/drivers/etc/hosts i w ostatniej linii dopisz:
```
#!txt

127.0.0.1.1.1	hma.comarch.pl
```
9.	Dodaj w IIS nową witrynę do sieci

*	Nazwa witryny: hma.comarch.pl
*	Ścieżka fizyczna: Sites/HmaProxy
*	Typ powiązania: https
*	Nazwa hosta: hma.comarch.pl
*	Wybrać certyfikat hma.comarch.pl

10.	W pliku server.js zmień adresy wskazujące Zdalną Pielęgniarkę
11.	Zainstaluj urlrewrite i zrestartuj komputer
12.	Po odpaleniu maszyny wirtualnej i odczekaniu ok. 6 minut (potrzebnych na załadowanie Bundles) wpisz w przeglądarce: https://hma.comarch.pl/rest/pairing/devices Jeśli będzie zwrócony json to konfiguracja przebiegła poprawnie